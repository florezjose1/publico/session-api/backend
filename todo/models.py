from django.db import models
from django.utils.translation import ugettext_lazy as _

from authentication.models import CustomUser

CHOICE_STATE = (
    ('new', _('New')),
    ('on_hold', _('On hold')),
    ('in_progress', _('In progress')),
    ('completed', _('Completed'))
)


class Task(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField()
    state = models.CharField(choices=CHOICE_STATE, max_length=15, null=False, blank=False, default='new')
    create_date = models.DateTimeField(null=False, blank=False, auto_now_add=True, verbose_name=_('Creation date'))
    limit_date = models.DateTimeField(null=False, blank=False, auto_now_add=True, verbose_name=_('Creation date'))
    color = models.CharField(verbose_name=_('Color'), null=False, blank=False, default='#000000', max_length=8)
    private = models.BooleanField(default=True)
