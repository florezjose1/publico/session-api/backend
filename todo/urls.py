from django.urls import path, include
from rest_framework import routers
from todo import views as v

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

router = routers.DefaultRouter()
router.register(r'task', v.TaskViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
