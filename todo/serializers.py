from rest_framework import serializers

from todo.models import Task


class TaskSerializer(serializers.HyperlinkedModelSerializer):
    create_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    limit_date = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")

    class Meta:
        model = Task
        fields = ['url', 'id', 'user', 'title', 'description', 'state', 'color', 'create_date', 'limit_date']
