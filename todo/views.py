from rest_framework import viewsets, status
from rest_framework.response import Response
from todo.models import Task
from todo.serializers import TaskSerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer

    def list(self, request, *args, **kwargs):
        queryset = Task.objects.filter(user=request.user)
        serializer_context = {
            'request': request,
        }
        serializer = TaskSerializer(queryset, many=True, context=serializer_context)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        data = request.data
        task = Task(user=request.user, title=data['title'], description=data['description'], color=data['color'],
                    limit_date=data['limit_date'])
        task.save()
        if task:
            serializer_context = {
                'request': request,
            }
            serializer = TaskSerializer(task, many=False, context=serializer_context)
            if serializer:
                return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(status=status.HTTP_400_BAD_REQUEST)

