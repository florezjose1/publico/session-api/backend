# Session by API

Versión Python: `^3.7`

Versión django: `3.0.05`


#### Configuración del proyecto

* Clone repositorio
```
$ git clone 
$ cd backend
```

* Cree virtulenv
```
$ virtualenv -p python3 venv
$ source venv/bin/activated
```

* Instale requerimientos
```
pip3 install -r requeriments.txt
```

* Ejecutar
```
python3 manage.py runserve
```


Hecho con ♥ por [Jose Florez](www.joseflorez.co)
