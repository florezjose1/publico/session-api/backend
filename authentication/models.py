from django.db import models
from django.contrib.auth.models import AbstractUser


def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return 'curriculum/user_{0}/{1}'.format(instance.username, filename)


class CustomUser(AbstractUser):
    number_identification = models.CharField(blank=True, max_length=20)
    country = models.CharField(blank=True, max_length=100)
    phone = models.CharField(blank=True, max_length=50)
    curriculum_vitae = models.FileField(upload_to=user_directory_path)
